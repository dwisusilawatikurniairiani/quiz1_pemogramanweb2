<?php

$panjang = 10;
$lebar = 5;
$luas = $panjang * $lebar;

echo "Panjang : " . $panjang . "<br>";
echo "Lebar : " . $lebar . "<br>";
echo "Luas : " . $luas . "<br>";

//menghitung transaksi jual beli
echo "==================================" . "<br>";
$harga_baju = 10000;
$harga_celana = 12000;
$jumlah_baju = 5;
$jumlah_celana = 3;
$total_belanja = ($harga_baju * $jumlah_baju) + ($harga_celana * $jumlah_celana);
$diskon;

if ($total_belanja > 50000) {
    $diskon = $total_belanja * 0.1;
} else {
    $diskon = 0;
}
$total_bayar = ($total_belanja + $diskon);

echo "Harga baju = " . $harga_baju . "<br>";
echo "Jumlah baju = " . $jumlah_baju . "<br>";
echo "Harga celana = " . $harga_celana . "<br>";
echo "Jumlah celana = " . $jumlah_celana . "<br>";
echo "Total belanja = " . $total_belanja . "<br>";
echo "Diskon = " . $diskon . "<br>";
echo "Total bayar = " . $total_bayar;
// ((int)$total_belanja + (int)$diskon)

echo "<h1>Tabel Belanja</h1>";
// awal tabel
echo "
<table border='1'>
<tr align='center'>
    <th>No</th>
    <th>Produk</th>
    <th>Harga</th>
    <th>Jumlah</th>
    <th>Total Belanja</th>
    <th>Diskon</th>
    <th>Total Bayar</th>
    
</tr>
<tr>
    <td>1</td>
    <td>Baju</td>
    <td>$harga_baju</td>
    <td>$jumlah_baju</td>
    <td>$total_belanja</td>
    <td>$diskon</td>
    <td>$total_bayar</td>
</tr>
<tr>
    <td>2</td>
    <td>Celana</td>
    <td>$harga_celana</td>
    <td>$jumlah_celana</td>
    <td>$total_belanja</td>
    <td>$diskon</td>
    <td>$total_bayar</td>
</tr>
<tr>
    <td colspan='6' align='center' >Total Belanja : </td>
    <td>$total_belanja</td>
</tr>
<tr>
    <td colspan='6' align='center' >Total Bayar</td>
    <td>$total_bayar</td>
</tr>
</table>
";
// akhir tabel